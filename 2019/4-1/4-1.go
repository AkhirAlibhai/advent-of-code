package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func isNumberValid(i int) int {
	currMin := 10
	currDigit := 10
	foundDouble := 0

	// To loop through each digit
	for {
		// If there are no more digits
		if i <= 0 {
			break
		}

		// If we have seen the same digit twice in a row
		if i%10 == currDigit {
			foundDouble = 1
		}

		currDigit = i % 10

		// If our digit is larger than a digit to the right of it
		if currDigit > currMin {
			return 0
		}

		// It's either the same, or smaller
		currMin = currDigit

		// Moving to the next digit
		i /= 10
	}

	return foundDouble
}

func main() {
	fileData, _ := ioutil.ReadFile("data.txt")

	data := strings.Split(string(fileData), "\n")

	lowerBound, _ := strconv.Atoi(data[0])
	upperBound, _ := strconv.Atoi(data[1])

	counter := 0

	for i := lowerBound; i <= upperBound; i++ {
		counter += isNumberValid(i)
	}

	fmt.Print(counter, "\n")
}
