package main

import (
	"fmt"
	"io/ioutil"
	"strings"
)

func getOrbitCount(orbits map[string][]string, curr string, count int) int {
	returnVal := 0
	if len(orbits[curr]) == 0 {
		return 0
	}
	for _, orbiter := range orbits[curr] {
		returnVal += count + getOrbitCount(orbits, orbiter, count+1)
	}
	return returnVal
}

func main() {
	fileData, _ := ioutil.ReadFile("data.txt")

	data := strings.Split(string(fileData), "\n")
	orbitData := make(map[string][]string)

	// Parsing the daya
	for _, currData := range data {
		currOrbit := strings.Split(currData, ")")

		// Appending the orbit
		orbitData[currOrbit[0]] = append(orbitData[currOrbit[0]], currOrbit[1])
	}

	fmt.Println(getOrbitCount(orbitData, "COM", 1))
}
