package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
)

func main() {
	fileData, _ := ioutil.ReadFile("data.txt")

	data := strings.Split(string(fileData), ",")
	intData := []int{}

	// Typecasting the whole array
	for _, currData := range data {
		currInt, _ := strconv.Atoi(currData)
		intData = append(intData, currInt)
	}

	// Keeping a copy of the default state
	defaultState := intData

	// Looping through each possible initialization values
	for x := 0; x <= 99; x++ {
		for y := 0; y <= 99; y++ {
			// Reseting the data
			intData = make([]int, len(defaultState))
			copy(intData, defaultState)

			// Initializing the data
			intData[1] = x
			intData[2] = y

			for i := 0; i < len(intData); i = i + 4 {
				switch intData[i] {
				case 1:
					intData[intData[i+3]] = intData[intData[i+1]] + intData[intData[i+2]]
				case 2:
					intData[intData[i+3]] = intData[intData[i+1]] * intData[intData[i+2]]
				case 99:
					// If we have found the output we want
					if intData[0] == 19690720 {
						fmt.Print(100*x+y, "\n")
						os.Exit(0)
					}
				}
			}
		}
	}
}
