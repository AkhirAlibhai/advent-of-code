package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func isNumberValid(i int) int {
	currMin := 10
	currDigit := 10
	foundDouble := map[int]bool{}

	// To loop through each digit
	for {
		// If there are no more digits
		if i <= 0 {
			break
		}

		// If we have seen the same digit twice in a row
		if i%10 == currDigit {
			// Since the digit appeared 3 times in a row, it is now invalid
			if _, exists := foundDouble[currDigit]; exists {
				foundDouble[currDigit] = false
			} else {
				foundDouble[currDigit] = true
			}
		}

		currDigit = i % 10

		// If our digit is larger than a digit to the right of it
		if currDigit > currMin {
			return 0
		}

		// It's either the same, or smaller
		currMin = currDigit

		// Moving to the next digit
		i /= 10
	}

	// Checking if we have any true values
	for _, x := range foundDouble {
		if x == true {
			return 1
		}
	}
	return 0
}

func main() {
	fileData, _ := ioutil.ReadFile("data.txt")

	data := strings.Split(string(fileData), "\n")

	lowerBound, _ := strconv.Atoi(data[0])
	upperBound, _ := strconv.Atoi(data[1])

	counter := 0

	for i := lowerBound; i <= upperBound; i++ {
		counter += isNumberValid(i)
	}

	fmt.Print(counter, "\n")
}
