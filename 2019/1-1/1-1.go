package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {
	fileData, _ := ioutil.ReadFile("data.txt")

	fuel := 0

	data := strings.Split(string(fileData), "\n")

	for i := 0; i < len(data); i++ {
		value, _ := strconv.Atoi(data[i])

		fuel += value/3 - 2
	}
	fmt.Print(fuel, "\n")
}
