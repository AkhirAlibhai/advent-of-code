package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
)

func main() {
	fileData, _ := ioutil.ReadFile("data.txt")

	data := strings.Split(string(fileData), ",")
	intData := []int{}

	// Typecasting the whole array
	for _, currData := range data {
		currInt, _ := strconv.Atoi(currData)
		intData = append(intData, currInt)
	}

	for i := 0; i < len(intData); i = i + 4 {
		modes := []int{}
		currInstruction := intData[i]
		optCode := 0

		counter := 0
		// To loop through each digit
		for {
			// If there are no more digits
			if currInstruction <= 0 {
				break
			}

			currDigit := currInstruction % 10

			switch counter {
			// The optcode
			case 0:
				optCode = currDigit
			case 1:
				if optCode == 9 && currDigit == 9 {
					optCode = 99
				}
			// Appending the digit so we know what mode it is in
			default:
				modes = append(modes, currDigit)
			}

			// Moving to the next digit
			currInstruction /= 10
			counter++
		}

		// Making sure the modes array has enough input for all possible parameters
		for {
			if len(modes) > 2 {
				break
			}
			modes = append(modes, 0)
		}
		switch optCode {
		case 1:
			// Parsing the modes data
			for j := range modes {
				if modes[j] == 0 {
					modes[j] = intData[intData[i+1+j]]
				} else {
					modes[j] = intData[i+1+j]
				}
			}
			intData[intData[i+3]] = modes[0] + modes[1]
		case 2:
			// Parsing the modes data
			for j := range modes {
				if modes[j] == 0 {
					modes[j] = intData[intData[i+1+j]]
				} else {
					modes[j] = intData[i+1+j]
				}
			}
			intData[intData[i+3]] = modes[0] * modes[1]
		case 3:
			intData[intData[i+1]] = 1
			i -= 2
		case 4:
			// Parsing the modes data
			if modes[0] == 0 {
				modes[0] = intData[intData[i+1]]
			} else {
				modes[0] = intData[i+1]
			}
			fmt.Println(modes[0])
			i -= 2
		case 99:
			os.Exit(0)
		}
	}
}
