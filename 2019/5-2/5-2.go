package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
)

func fixSizeOfModes(modes []int, size int) []int {
	for {
		if len(modes) >= size {
			if len(modes) == size {
				return modes
			}
			modes = modes[:len(modes)-1]
		} else {
			modes = append(modes, 0)
		}
	}
}

func main() {
	fileData, _ := ioutil.ReadFile("data.txt")

	data := strings.Split(string(fileData), ",")
	intData := []int{}

	// Typecasting the whole array
	for _, currData := range data {
		currInt, _ := strconv.Atoi(currData)
		intData = append(intData, currInt)
	}

	for i := 0; i < len(intData); {
		modes := []int{}
		currInstruction := intData[i]
		optCode := 0

		counter := 0
		// To loop through each digit
		for {
			// If there are no more digits
			if currInstruction <= 0 {
				break
			}

			currDigit := currInstruction % 10

			switch counter {
			// The optcode
			case 0:
				optCode = currDigit
			case 1:
				if optCode == 9 && currDigit == 9 {
					optCode = 99
				}
			// Appending the digit so we know what mode it is in
			default:
				modes = append(modes, currDigit)
			}

			// Moving to the next digit
			currInstruction /= 10
			counter++
		}

		switch optCode {
		case 1:
			modes = fixSizeOfModes(modes, 3)
			for j := range modes {
				if modes[j] == 0 {
					modes[j] = intData[intData[i+1+j]]
				} else {
					modes[j] = intData[i+1+j]
				}
			}
			intData[intData[i+3]] = modes[0] + modes[1]
			i += 4
		case 2:
			modes = fixSizeOfModes(modes, 3)
			for j := range modes {
				if modes[j] == 0 {
					modes[j] = intData[intData[i+1+j]]
				} else {
					modes[j] = intData[i+1+j]
				}
			}
			intData[intData[i+3]] = modes[0] * modes[1]
			i += 4
		case 3:
			intData[intData[i+1]] = 5
			i += 2
		case 4:
			modes = fixSizeOfModes(modes, 1)
			if modes[0] == 0 {
				modes[0] = intData[intData[i+1]]
			} else {
				modes[0] = intData[i+1]
			}
			fmt.Println(modes[0])
			i += 2
		case 5:
			modes = fixSizeOfModes(modes, 2)
			for j := range modes {
				if modes[j] == 0 {
					modes[j] = intData[intData[i+1+j]]
				} else {
					modes[j] = intData[i+1+j]
				}

				if modes[0] == 0 {
					break
				}
			}
			if modes[0] != 0 {
				// - 3 to account for the + 3
				i = modes[1] - 3
			}
			i += 3
		case 6:
			modes = fixSizeOfModes(modes, 2)
			for j := range modes {
				if modes[j] == 0 {
					modes[j] = intData[intData[i+1+j]]
				} else {
					modes[j] = intData[i+1+j]
				}

				if modes[0] != 0 {
					break
				}
			}
			if modes[0] == 0 {
				// - 3 to account for the + 3
				i = modes[1] - 3
			}
			i += 3
		case 7:
			modes = fixSizeOfModes(modes, 3)
			for j := range modes {
				if modes[j] == 0 {
					modes[j] = intData[intData[i+1+j]]
				} else {
					modes[j] = intData[i+1+j]
				}
			}
			if modes[0] < modes[1] {
				intData[intData[i+3]] = 1
			} else {
				intData[intData[i+3]] = 0
			}
			i += 4
		case 8:
			modes = fixSizeOfModes(modes, 3)
			for j := range modes {
				if modes[j] == 0 {
					modes[j] = intData[intData[i+1+j]]
				} else {
					modes[j] = intData[i+1+j]
				}
			}
			if modes[0] == modes[1] {
				intData[intData[i+3]] = 1
			} else {
				intData[intData[i+3]] = 0
			}
			i += 4
		case 99:
			os.Exit(0)
		}
	}
}
