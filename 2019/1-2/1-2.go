package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func calculateFuel(value int) int {
	fuel := value/3 - 2

	if fuel <= 0 {
		return 0
	}

	return fuel + calculateFuel(fuel)
}

func main() {
	fileData, _ := ioutil.ReadFile("data.txt")

	fuel := 0

	data := strings.Split(string(fileData), "\n")

	for i := 0; i < len(data); i++ {
		value, _ := strconv.Atoi(data[i])

		fuel += calculateFuel(value)
	}
	fmt.Print(fuel, "\n")
}
