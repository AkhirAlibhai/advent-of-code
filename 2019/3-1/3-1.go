package main

import (
	"fmt"
	"io/ioutil"
	"math"
	"strconv"
	"strings"
)

type tuple struct {
	x int
	y int
}

func (t tuple) Sum(a tuple) tuple {
	t.x += a.x
	t.y += a.y

	return t
}

func evaluteWire(grid map[int]map[int]int, wire []string, wireValue int) map[int]map[int]int {
	// Creating our current point tracker
	currPoint := tuple{x: 0, y: 0}

	// Setting our direction
	direction := tuple{x: 0, y: 0}

	// For each instruction
	for _, instruction := range wire {
		// Getting the direction
		switch string(instruction[0]) {
		case "U":
			direction = tuple{x: 0, y: 1}
		case "D":
			direction = tuple{x: 0, y: -1}
		case "L":
			direction = tuple{x: -1, y: 0}
		case "R":
			direction = tuple{x: 1, y: 0}
		}

		// How far we go in the given direction
		currCount, _ := strconv.Atoi(instruction[1:])

		// Moving that much
		for i := 0; i < currCount; i++ {
			currPoint = currPoint.Sum(direction)

			// Creating a new dictionary
			if _, exists := grid[currPoint.x]; !exists {
				grid[currPoint.x] = make(map[int]int)
			}

			// If the point has not been visited
			if _, exists := grid[currPoint.x][currPoint.y]; !exists {
				grid[currPoint.x][currPoint.y] = wireValue
			} else {
				// If the point has not been visited by the wire that was already there
				// This only works since we have 2 wires
				if grid[currPoint.x][currPoint.y] != wireValue {
					grid[currPoint.x][currPoint.y] += wireValue
				}
			}

		}
	}
	return grid
}

func main() {
	fileData, _ := ioutil.ReadFile("data.txt")

	data := strings.Split(string(fileData), "\n")

	wireOne := strings.Split(data[0], ",")
	wireTwo := strings.Split(data[1], ",")

	wireOneValue := 1
	wireTwoValue := 2

	// Creating the grid
	wireGrid := make(map[int]map[int]int)

	wireGrid = evaluteWire(wireGrid, wireOne, wireOneValue)
	wireGrid = evaluteWire(wireGrid, wireTwo, wireTwoValue)

	// Setting our distance to the largest it could possibly be
	bestDistance := math.Inf(1)

	for x, e := range wireGrid {
		for y, cross := range e {
			if cross == (wireOneValue + wireTwoValue) {
				if bestDistance > (math.Abs(float64(x)) + math.Abs(float64(y))) {
					bestDistance = (math.Abs(float64(x)) + math.Abs(float64(y)))
				}
			}
		}
	}
	fmt.Print(bestDistance, "\n")
}
