package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

func getOrbitsInPath(orbits map[string]string, src string) []string {
	if _, exists := orbits[src]; !exists {
		return make([]string, 0)
	}
	return append(getOrbitsInPath(orbits, orbits[src]), orbits[src])
}

func main() {
	fileData, _ := ioutil.ReadFile("data.txt")

	data := strings.Split(string(fileData), "\n")
	orbitData := make(map[string]string)

	// Parsing the daya
	for _, currData := range data {
		currOrbit := strings.Split(currData, ")")

		// Appending the orbit
		orbitData[currOrbit[1]] = currOrbit[0]
	}

	youPath := getOrbitsInPath(orbitData, "YOU")
	sanPath := getOrbitsInPath(orbitData, "SAN")

	for i := len(youPath) - 1; i >= 0; i-- {
		for j := len(sanPath) - 1; j >= 0; j-- {
			// The first time their orbits intersect
			if youPath[i] == sanPath[j] {
				// - 2 to not count the one where they intersect twice, and the start orbits
				fmt.Println(len(youPath) - i - 2 + len(sanPath) - j)
				os.Exit(0)
			}
		}
	}
}
