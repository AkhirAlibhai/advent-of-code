package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {
	fileData, _ := ioutil.ReadFile("data.txt")

	data := strings.Split(string(fileData), ",")
	intData := []int{}

	// Typecasting the whole array
	for _, currData := range data {
		currInt, _ := strconv.Atoi(currData)
		intData = append(intData, currInt)
	}

	// The question said to do this
	intData[1] = 12
	intData[2] = 2

	for i := 0; i < len(intData); i = i + 4 {
		switch intData[i] {
		case 1:
			intData[intData[i+3]] = intData[intData[i+1]] + intData[intData[i+2]]
		case 2:
			intData[intData[i+3]] = intData[intData[i+1]] * intData[intData[i+2]]
		case 99:
			fmt.Print(intData[0], "\n")
			break
		}
	}
}
