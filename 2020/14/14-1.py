def computeMask(mask, val):
    binString = bin(val)[2:].zfill(36)
    return int(''.join([mask[x] if mask[x] == "0" or mask[x] == "1" else binString[x] for x in range(0, 36)]), 2)

data = []

for line in open("input.txt", "r"):
    data.append(line[:-1])

mem = {}
for line in data:
    lineSplit = line.split(" = ")
    if lineSplit[0] == "mask":
        mask = lineSplit[1]
    else:
        mem[lineSplit[0][4:-1]] = computeMask(mask, int(lineSplit[1]))

print(sum([mem[x] for x in mem]))