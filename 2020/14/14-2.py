def computeMem(mask, val):
    output = [""]
    binString = bin(val)[2:].zfill(36)

    for x in range(0, 36):
        if mask[x] == "0":
            output = [line + binString[x] for line in output]
        elif mask[x] == "1":
            output = [line + "1" for line in output]
        else:
            newOutput = []
            for line in output:
                newOutput.append(line + "0")
                newOutput.append(line + "1")
            output = newOutput

    return [int(x, 2) for x in output]

data = []

for line in open("input.txt", "r"):
    data.append(line[:-1])

mem = {}
for line in data:
    lineSplit = line.split(" = ")
    if lineSplit[0] == "mask":
        mask = lineSplit[1]
    else:
        for memoryLocation in computeMem(mask, int(lineSplit[0][4:-1])):
            mem[memoryLocation] = int(lineSplit[1])

print(sum([mem[x] for x in mem]))