data = []

for line in open("input.txt", "r"):
    data.append(line.strip("\n"))

everything = []
allergens = {}
recipes = []
for line in data:
    splitLine = line.split(" (contains ")
    currIngredients = splitLine[0].split(" ")
    currAllergens = splitLine[1].strip(")").split(", ")
    everything += currIngredients

    for allergen in currAllergens:
        if allergen in allergens:
            allergens[allergen].union(set(currIngredients))
        else:
            allergens[allergen] = set(currIngredients)
    recipes.append((currAllergens, currIngredients))

for recipe in recipes:
    currAllergens = recipe[0]
    currIngredients = recipe[1]
    for allergen in currAllergens:
        if allergen in allergens:
            removeMe = set()
            for ingredient in allergens[allergen]:
                if ingredient not in currIngredients:
                    if len(allergens[allergen]) != 1:
                        removeMe.add(ingredient)
            [allergens[allergen].remove(x) for x in removeMe]
        else:
            allergens[allergen] = currIngredients

trueAllergens = set()
while True:
    for allergen in allergens:
        if len(allergens[allergen]) == 1:
            trueAllergens.add(list(allergens[allergen])[0])
        else:
            allergens[allergen] = [x for x in allergens[allergen] if x not in trueAllergens]
    if sum([1 for x in allergens for y in allergens[x]]) == len(allergens):
        break

output = []
for x in sorted(allergens.keys()):
    output += list(allergens[x])

print(','.join(output))
