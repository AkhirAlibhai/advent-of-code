def rotateLeft(curr, val):
    if val == 0:
        return curr
    return rotateLeft((curr[1], -1*curr[0]), val - 90)

def rotateRight(curr, val):
    if val == 0:
        return curr
    return rotateRight((-1*curr[1], curr[0]), val - 90)

data = []

for line in open("input.txt", "r"):
    data.append(line[:-1])

vertical = 0
horizontal = 0
direction = (1, 10)

for move in data:
    if move[0] == "N":
        direction = (direction[0] + int(move[1:]), direction[1])
    elif move[0] == "S":
        direction = (direction[0] - int(move[1:]), direction[1])
    elif move[0] == "E":
        direction = (direction[0], direction[1] + int(move[1:]))
    elif move[0] == "W":
        direction = (direction[0], direction[1] - int(move[1:]))
    elif move[0] == "L":
        direction = rotateLeft(direction, int(move[1:]))
    elif move[0] == "R":
        direction = rotateRight(direction, int(move[1:]))
    elif move[0] == "F":
        vertical += direction[0] * int(move[1:])
        horizontal += direction[1] * int(move[1:])

print(abs(vertical) + abs(horizontal))