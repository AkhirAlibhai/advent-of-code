def rotate(val):
    if val == 0:
        return 0
    else:
        return 1 + rotate(val - 90)

data = []

for line in open("input.txt", "r"):
    data.append(line[:-1])

vertical = 0
horizontal = 0
direction = 1

for move in data:
    if move[0] == "N":
        vertical += int(move[1:])
    elif move[0] == "S":
        vertical -= int(move[1:])
    elif move[0] == "E":
        horizontal += int(move[1:])
    elif move[0] == "W":
        horizontal -= int(move[1:])
    elif move[0] == "L":
        direction -= rotate(int(move[1:]))
        direction = direction % 4
    elif move[0] == "R":
        direction += rotate(int(move[1:]))
        direction = direction % 4
    elif move[0] == "F":
        if direction == 0:
            vertical += int(move[1:])
        elif direction == 1:
            vertical -= int(move[1:])
        elif direction == 2:
            horizontal += int(move[1:])
        elif direction == 3:
            horizontal -= int(move[1:])

print(abs(vertical) + abs(horizontal))