data = []

for line in open("input.txt", "r"):
    data.append(line.strip("\n"))

directions = {
                "e": (0, 1, 1),
                "w": (0, -1, 1),
                "ne": (1, 0.5, 2),
                "nw": (1, -0.5, 2),
                "se": (-1, 0.5, 2),
                "sw": (-1, -0.5, 2)
            }

tiles = {}
tiles[(0, 0)] = False
for instruction in data:
    currEast = 0
    currNorth = 0
    currIndex = 0

    currMove = None
    while currIndex < len(instruction):
        if instruction[currIndex] == "e" or instruction[currIndex] == "w":
            currMove = directions[instruction[currIndex]]
        else:
            currMove = directions[instruction[currIndex:currIndex+2]]

        currNorth += currMove[0]
        currEast += currMove[1]
        currIndex += currMove[2]

        if (currNorth, currEast) not in tiles:
            tiles[(currNorth, currEast)] = False

    tiles[(currNorth, currEast)] = not tiles[(currNorth, currEast)]

print(sum([1 for x in tiles if tiles[x]]))