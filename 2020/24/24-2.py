data = []

for line in open("input.txt", "r"):
    data.append(line.strip("\n"))

directions = {
                "e": (0, 1, 1),
                "w": (0, -1, 1),
                "ne": (1, 0.5, 2),
                "nw": (1, -0.5, 2),
                "se": (-1, 0.5, 2),
                "sw": (-1, -0.5, 2)
            }

tiles = {}
tiles[(0, 0)] = False
for instruction in data:
    currEast = 0
    currNorth = 0
    currIndex = 0

    currMove = None
    while currIndex < len(instruction):
        if instruction[currIndex] == "e" or instruction[currIndex] == "w":
            currMove = directions[instruction[currIndex]]
        else:
            currMove = directions[instruction[currIndex:currIndex+2]]

        currNorth += currMove[0]
        currEast += currMove[1]
        currIndex += currMove[2]

        if (currNorth, currEast) not in tiles:
            tiles[(currNorth, currEast)] = False

    tiles[(currNorth, currEast)] = not tiles[(currNorth, currEast)]

dims = [(0, 1), (0, -1), (1, 0.5), (1, -0.5), (-1, 0.5), (-1, -0.5)]
for x in range(0, 100):
    addedTiles = []
    newTiles = tiles.copy()

    for tile in tiles:
        currCount = 0
        for (north, east) in dims:
            currNorth = tile[0] + north
            currEast = tile[1] + east
            if (currNorth, currEast) not in tiles:
                newTiles[(currNorth, currEast)] = False
                addedTiles.append((currNorth, currEast))
            elif tiles[(currNorth, currEast)]:
                currCount += 1
        if not tiles[tile] and currCount == 2:
            newTiles[tile] = True
        elif tiles[tile] and (currCount > 2 or currCount == 0):
            newTiles[tile] = False

    for tile in addedTiles:
        currCount = 0
        for (north, east) in dims:
            currNorth = tile[0] + north
            currEast = tile[1] + east
            if (currNorth, currEast) in tiles:
                if tiles[(currNorth, currEast)]:
                    currCount += 1
        if currCount == 2:
            newTiles[tile] = True
    tiles = newTiles

print(sum([1 for x in tiles if tiles[x]]))