data = [0]

for line in open("input.txt", "r"):
    data.append(int(line[:-1]))

data.sort()

runs = [0 for x in data]
runs[0] = 1
x = 0
while x < len(data):
    if x + 1 < len(data) and data[x + 1] <= data[x] + 3:
        runs[x+1] += runs[x]
        if x + 2 < len(data) and data[x + 2] <= data[x] + 3:
            runs[x+2] += runs[x]
            if x + 3 < len(data) and data[x + 3] <= data[x] + 3:
                runs[x+3] += runs[x]

    x += 1

print(runs[-1])