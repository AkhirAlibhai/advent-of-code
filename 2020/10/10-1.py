data = []

for line in open("input.txt", "r"):
    data.append(int(line[:-1]))

data.sort()

one = 1
three = 1
for x in range(1, len(data)):
    if data[x] > data[x-1] + 4:
        break
    if data[x] - data[x-1] == 1:
        one += 1
    elif data[x] - data[x-1] == 3:
        three += 1

print(one*three)