import math

def decodeSeat(line):
    rowMin = 0
    rowMax = 127
    for x in range(0, 7):
        if line[x] == "F":
            rowMax -= math.ceil((rowMax - rowMin) / 2)
        elif line[x] == "B":
            rowMin += math.ceil((rowMax - rowMin) / 2)

    colMin = 0
    colMax = 7
    for x in range(7, 10):
        if line[x] == "L":
            colMax -= math.ceil((colMax - colMin) / 2)
        elif line[x] == "R":
            colMin += math.ceil((colMax - colMin) / 2)

    return (rowMin * 8 + colMin)

foundIds = set()

for line in open("input.txt", "r"):
    foundIds.add(decodeSeat(line[:-1]))

sortedIds = sorted(foundIds)

prevId = sortedIds[0]
currId = sortedIds[1]

for id in sortedIds[2:]:
    if prevId + 1 != currId:
        print(prevId + 1)
        break
    prevId = currId
    currId = id