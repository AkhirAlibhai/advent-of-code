import math

def decodeSeat(line):
    rowMin = 0
    rowMax = 127
    for x in range(0, 7):
        if line[x] == "F":
            rowMax -= math.ceil((rowMax - rowMin) / 2)
        elif line[x] == "B":
            rowMin += math.ceil((rowMax - rowMin) / 2)

    colMin = 0
    colMax = 7
    for x in range(7, 10):
        if line[x] == "L":
            colMax -= math.ceil((colMax - colMin) / 2)
        elif line[x] == "R":
            colMin += math.ceil((colMax - colMin) / 2)

    return (rowMin * 8 + colMin)

currMax = -1

for line in open("input.txt", "r"):
    currVal = decodeSeat(line[:-1])
    if currVal > currMax:
        currMax = currVal

print(currMax)