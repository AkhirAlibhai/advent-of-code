answeredQuestions = set()
total = 0

for line in open("input.txt", "r"):
    if line == '\n':
        # We have a new group
        total += len(answeredQuestions)
        answeredQuestions = set()
    else:
        for question in line[:-1]:
            answeredQuestions.add(question)

total += len(answeredQuestions)

print(total)