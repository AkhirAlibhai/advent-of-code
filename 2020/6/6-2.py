from collections import defaultdict

peopleInGroup = 0
answeredQuestions = defaultdict(int)
total = 0

for line in open("input.txt", "r"):
    if line == '\n':
        # We have a new group
        for question in answeredQuestions:
            if answeredQuestions[question]/peopleInGroup == 1:
                total += 1
        peopleInGroup = 0
        answeredQuestions = defaultdict(int)
    else:
        peopleInGroup += 1
        for question in line[:-1]:
            answeredQuestions[question] += 1

for question in answeredQuestions:
    if answeredQuestions[question]/peopleInGroup == 1:
        total += 1

print(total)