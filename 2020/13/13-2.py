from sympy.ntheory.modular import crt

data = []

for line in open("input.txt", "r"):
    data.append(line[:-1])

earliest = int(data[0])

times = []
offset = 0
for time in data[1].split(","):
    if time != "x":
        times.append((int(time), offset))
    offset += 1

print(crt([time for (time, _) in times], [(time - offset) for (time, offset) in times])[0])
