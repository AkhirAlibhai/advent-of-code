data = []

for line in open("input.txt", "r"):
    data.append(line[:-1])

earliest = int(data[0])

times = []

for time in data[1].split(","):
    if time != "x":
        times.append(int(time))

bestId = 0
bestTime = None
for time in times:
    currTime = 0
    while currTime < earliest:
        currTime += time
    if bestTime == None or bestTime > currTime:
        bestTime = currTime
        bestId = time
print(bestId * (bestTime - earliest))