def getBag(bag):
    if not bag.endswith("s"):
        bag = bag + "s"

    if data[bag][0] == "no other bags":
        return 0

    count = 0
    for innerBag in data[bag]:
        count += int(innerBag[0]) + int(innerBag[0])*getBag(innerBag[2:])
    return count

data = {}

for line in open("input.txt", "r"):
        splitLine = line[:-1].split(" contain ")
        inners = splitLine[1][:-1].split(", ")
        data[splitLine[0]] = inners

count = getBag("shiny gold bags")

print(count)