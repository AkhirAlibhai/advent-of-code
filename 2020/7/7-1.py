data = dict()

for line in open("input.txt", "r"):
        splitLine = line[:-1].split(" contain ")
        inners = splitLine[1][:-1].split(", ")
        data[splitLine[0]] = inners

newHasShinyGold = []
shinyGold = {"shinygold"}

while True:
    oldLen = len(shinyGold)
    for bag in data:
        for innerBags in data[bag]:
            splitInnerBags = innerBags.split(" ")
            if f"{splitInnerBags[1]}{splitInnerBags[2]}" in shinyGold:
                shinyGold.add(f"{bag.split(' ')[0]}{bag.split(' ')[1]}")
                break
    if oldLen == len(shinyGold):
        break

print(len(shinyGold) - 1)