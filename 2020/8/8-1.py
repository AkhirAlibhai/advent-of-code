data = []

for line in open("input.txt", "r"):
    data.append(line[:-1])

accumulator = 0

currIndex = 0
visitedLines = set()

while True:
    if currIndex in visitedLines:
        print(accumulator)
        break
    visitedLines.add(currIndex)
    splitLine = data[currIndex].split(" ")
    if splitLine[0] == "acc":
        accumulator += int(splitLine[1])
        currIndex += 1
    elif splitLine[0] == "jmp":
        currIndex += int(splitLine[1])
    else:
        currIndex += 1