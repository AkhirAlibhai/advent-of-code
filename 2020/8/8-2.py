def runGame(data):
    accumulator = 0
    currIndex = 0
    visitedLines = set()

    while True:
        if currIndex in visitedLines:
            return None
        visitedLines.add(currIndex)
        try:
            splitLine = data[currIndex].split(" ")
            if splitLine[0] == "acc":
                accumulator += int(splitLine[1])
                currIndex += 1
            elif splitLine[0] == "jmp":
                currIndex += int(splitLine[1])
            else:
                currIndex += 1
        except IndexError:
            return accumulator

data = []

for line in open("input.txt", "r"):
    data.append(line[:-1])

for x in range(0, len(data)):
    splitLine = data[x].split(" ")
    val = None
    oldLine = data[x]

    if splitLine[0] == "jmp":
        data[x] = f"nop {splitLine[1]}"
        val = runGame(data)
    elif splitLine[0] == "nop":
        data[x] = f"jmp {splitLine[1]}"
        val = runGame(data)

    if val:
        print(val)
    data[x] = oldLine