import re

def buildResult(rule):
    if rule == '"a"' or rule == '"b"':
        return rule.strip('"')

    output = ""
    for currRule in rules[rule]:
        currMatches = ""
        for match in currRule:
            currMatch = buildResult(match)

            if not currMatches or len(currMatches) == 0:
                currMatches = currMatch
            else:
                currMatches += currMatch
        if output == "":
            output = currMatches
        else:
            output = f"({output})|({currMatches})"
    return f"({output})"

data = []

for line in open("input.txt", "r"):
    data.append(line.strip("\n"))

rules = {}

x = 0
while x < len(data):
    if data[x] == "":
        break
    ruleSplit = data[x].split(": ")
    rules[ruleSplit[0]] = []
    for branch in ruleSplit[1].split(" | "):
        rules[ruleSplit[0]].append(branch.split(" "))
    x += 1

matcher = re.compile(buildResult("0"))
print(sum([1 for rule in data[x + 1:] if matcher.fullmatch(rule)]))
