import re

def buildResult(rule):
    if rule == '"a"' or rule == '"b"':
        return rule.strip('"')

    output = ""
    for currRule in rules[rule]:
        currMatches = ""
        for match in currRule:
            currMatch = buildResult(match)

            if not currMatches or len(currMatches) == 0:
                currMatches = currMatch
            else:
                # This is a really bad solution, will only work up to 10 blocks
                # Did not want to learn how to do if statements in regex
                if rule == "11":
                    tmpMatches = currMatches
                    currMatches = f"((({currMatches})" + "{1}" + f"({currMatch})" + "{1})"
                    for i in range(2, 10):
                        currMatches += f"|(({tmpMatches})" + "{" +  f"{i}" + "}" + f"({currMatch})" + "{" +  f"{i}" + "})"
                    currMatches += ")"
                else:
                    currMatches += currMatch
        if output == "":
            output = currMatches
        else:
            output = f"({output})|({currMatches})"
    if rule == "8":
        output += "+"
    return f"({output})"

data = []

for line in open("input.txt", "r"):
    data.append(line.strip("\n"))

rules = {}

x = 0
while x < len(data):
    if data[x] == "":
        break
    ruleSplit = data[x].split(": ")
    rules[ruleSplit[0]] = []
    for branch in ruleSplit[1].split(" | "):
        rules[ruleSplit[0]].append(branch.split(" "))
    x += 1

# rules["8"] = [['42'], ['42', '8']]
# rules["11"] = [['42', '31'], ['42', '11', '31']]

matcher = re.compile(buildResult("0"))
print(sum([1 for rule in data[x + 1:] if matcher.fullmatch(rule)]))
