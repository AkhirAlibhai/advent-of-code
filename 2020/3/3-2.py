def getTrees(data, slopeX, slopeY):
    posX = 0
    posY = 0

    width = len(data[0])

    trees = 0

    while True:
        posX += slopeX
        posY += slopeY

        if posY >= len(data):
            return trees

        if data[posY][posX % width] == "#":
            trees += 1

data = []

for line in open("input.txt", "r"):
    data.append(line[:-1])

print(getTrees(data, 1, 1) *
        getTrees(data, 3, 1) *
        getTrees(data, 5, 1) *
        getTrees(data, 7, 1) *
        getTrees(data, 1, 2))