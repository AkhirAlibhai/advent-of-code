def evalExpression(line, start):
    recentOperation = ""
    currValue = 0
    firstTime = True

    x = start
    while x < len(line):
        if line[x] == "(":
            returnVal = evalExpression(line, x + 1)
            if firstTime:
                currValue = returnVal[0]
                firstTime = False
            else:
                if recentOperation == "+":
                    currValue += returnVal[0]
                elif recentOperation == "*":
                    currValue *= returnVal[0]
            x = returnVal[1]
        elif line[x] == ")":
            return (currValue, x)
        elif line[x] == "+" or line[x] == "*":
            recentOperation = line[x]
        else:
            if firstTime:
                currValue = int(line[x])
                firstTime = False
            else:
                if recentOperation == "+":
                    currValue += int(line[x])
                elif recentOperation == "*":
                    currValue *= int(line[x])
        x += 1
    return (currValue, x)

def preProcess(line, start):
    newLine = ""
    lastChunk = ""
    x = start
    inPlus = False

    while x < len(line):
        if line[x] == " ":
            pass
        else:
            if line[x] == "+":
                if inPlus:
                    lastChunk += ")"
                newLine += "(" + lastChunk + "+"
                lastChunk = ""
                inPlus = True
            elif line[x] == "(":
                returnVal = preProcess(line, x + 1)
                lastChunk += "(" + returnVal[0]
                x = returnVal[1]
            elif line[x] == ")":
                if inPlus:
                    lastChunk += ")"
                return (newLine + lastChunk + ")", x)
            elif line[x] == "*":
                if inPlus:
                    inPlus = False
                    newLine += lastChunk + ")*"
                    lastChunk = ""
                else:
                    lastChunk += "*"
                    newLine += lastChunk
                    lastChunk = ""
            else:
                lastChunk += line[x]
        x += 1
    if inPlus:
        lastChunk += ")"
    return (newLine + lastChunk, x)

data = []
for line in open("input.txt", "r"):
    data.append(line.strip("\n"))

print(sum([evalExpression(preProcess(line, 0)[0], 0)[0] for line in data]))
