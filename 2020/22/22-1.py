from collections import deque

playerOne = deque()
playerTwo = deque()
for line in open("input.txt", "r"):
    currLine = line.strip("\n")

    if currLine == "":
        pass
    elif currLine == "Player 1:":
        isPlayerOne = True
    elif currLine == "Player 2:":
        isPlayerOne = False
    else:
        if isPlayerOne:
            playerOne.append(int(currLine))
        else:
            playerTwo.append(int(currLine))

while len(playerOne) > 0 and len(playerTwo) > 0:
    currOne = playerOne.popleft()
    currTwo = playerTwo.popleft()

    if currOne > currTwo:
        playerOne.append(currOne)
        playerOne.append(currTwo)
    else:
        playerTwo.append(currTwo)
        playerTwo.append(currOne)

winner = []
if len(playerOne) == 0:
    winner = playerTwo
else:
    winner = playerOne

winner.reverse()

output = 0
for x in range(0, len(winner)):
    output += winner[x] * (x + 1)

print(output)
