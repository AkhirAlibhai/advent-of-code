from collections import deque

def playGame(playerOne, playerTwo):
    playerOneDecks = set()
    playerTwoDecks = set()

    while True:
        tupleOne = tuple(playerOne)
        tupleTwo = tuple(playerTwo)
        if tupleOne in playerOneDecks or tupleTwo in playerTwoDecks:
            return (playerOne, True)

        playerOneDecks.add(tupleOne)
        playerTwoDecks.add(tupleTwo)

        currOne = playerOne.popleft()
        currTwo = playerTwo.popleft()

        if len(playerOne) >= currOne and len(playerTwo) >= currTwo:
            if playGame(deque(list(playerOne)[:currOne]), deque(list(playerTwo)[:currTwo]))[1]:
                playerOne.append(currOne)
                playerOne.append(currTwo)
            else:
                playerTwo.append(currTwo)
                playerTwo.append(currOne)
        else:
            if currOne > currTwo:
                playerOne.append(currOne)
                playerOne.append(currTwo)
            else:
                playerTwo.append(currTwo)
                playerTwo.append(currOne)

        if len(playerOne) == 0:
            return (playerTwo, False)
        elif len(playerTwo) == 0:
            return (playerOne, True)

playerOne = deque()
playerTwo = deque()
for line in open("input.txt", "r"):
    currLine = line.strip("\n")

    if currLine == "":
        pass
    elif currLine == "Player 1:":
        isPlayerOne = True
    elif currLine == "Player 2:":
        isPlayerOne = False
    else:
        if isPlayerOne:
            playerOne.append(int(currLine))
        else:
            playerTwo.append(int(currLine))

winner = playGame(playerOne, playerTwo)[0]
winner.reverse()

output = 0
for x in range(0, len(winner)):
    output += winner[x] * (x + 1)

print(output)
