class Node:
    def __init__(self, data):
        self.data = data
        self.head = None

    def setHead(self, head):
        self.head = head

    def next(self, y):
        currNode = self
        for x in range(0, y):
            currNode = currNode.head
        return currNode

data = []

for line in open("input.txt", "r"):
    data.append(line.strip("\n"))

cups = [int(x) for x in data[0]]
cupsDict = {}
for x in range(max(cups) + 1, 1000000 + 1):
    cups.append(x)

prevNode = None
currNode = None
firstNode = None
for x in cups:
    currNode = Node(x)
    cupsDict[x] = currNode
    if firstNode == None:
        firstNode = currNode
    else:
        prevNode.setHead(currNode)
    prevNode = currNode

prevNode.setHead(firstNode)

currNode = firstNode
for x in range(0, 10000000):
    removedNodes = []
    for y in range(1, 4):
        removedNodes.append(currNode.next(y))
    removedData = [z.data for z in removedNodes]
    currNode.setHead(removedNodes[2].head)
    currCount = currNode.data - 1

    while currCount in removedData and currCount > 0:
        currCount -= 1

    if currCount <= 0:
        currCount = 1000000
        if currCount in removedData:
            currCount -= 1
            if currCount in removedData:
                currCount -= 1
                if currCount in removedData:
                    currCount -= 1

    newCurrNode = cupsDict[currCount]
    oldHead = newCurrNode.head
    newCurrNode.setHead(removedNodes[0])
    removedNodes[2].setHead(oldHead)
    
    currNode = currNode.head

print(cupsDict[1].next(1).data * cupsDict[1].next(2).data)
