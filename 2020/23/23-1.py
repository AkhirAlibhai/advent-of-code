data = []

for line in open("input.txt", "r"):
    data.append(line.strip("\n"))

cups = [int(x) for x in data[0]]

currIndex = 0
for x in range(0, 100):
    currCup = cups[currIndex % len(cups)]
    print(cups, currCup)
    removedCups = []
    for y in range(1, 4):
        removedCups.append(cups[(currIndex + y) % len(cups)])

    currCount = currCup - 1
    foundCup = None
    while currCount > 0:
        if currCount not in removedCups:
            foundCup = cups.index(currCount)
            break
        currCount -= 1

    if currCount == 0:
        foundCup = cups.index(max([z for z in cups if z not in removedCups]))
        
    newCups = []
    for count in range(0, len(cups)):
        if count == foundCup:
            newCups.append(cups[count])
            newCups += removedCups
        else:
            if cups[count] not in removedCups:
                newCups.append(cups[count])

    currIndex = (newCups.index(currCup) + 1) % len(newCups)            
    cups = newCups

output = ""
onePosition = cups.index(1)
for x in range(1, len(cups)):
    output += str(cups[(onePosition + x) % len(cups)])

print(output)
