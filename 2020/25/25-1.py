def performEncryption(value, subjectNumber, loops = 1):
    for x in range(0, loops):
        value = (value * subjectNumber) % 20201227
    return value

data = set()

for line in open("input.txt", "r"):
    data.add(int(line.strip("\n")))

loopVal = set()
for pubKey in data:
    value = 1
    loopCount = 0
    while True:
        value = performEncryption(value, 7)
        loopCount += 1
        
        if value == pubKey:
            loopVal.add(loopCount)
            break

output = set()
for pubKey in data:
    currOutput = set()
    for x in loopVal:
        currOutput.add(performEncryption(1, pubKey, x))
    if len(output) == 0:
        output = currOutput
    else:
        output = output.intersection(currOutput)

print([x for x in output][0])