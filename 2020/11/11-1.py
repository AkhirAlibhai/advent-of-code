import copy

def myCheck(l, m):
    for x in range(0, len(l)):
        for y in range(0, len(l[x])):
            if l[x][y] != m[x][y]:
                return False
    return True

def count(l):
    count = 0
    for x in l:
        for y in x:
            if y == "#":
                count += 1
    return count

def nextRound(l):
    output = copy.deepcopy(l)
    for x in range(0, len(l)):
        for y in range(0, len(l[x])):
            occupied = 0
            for (a, b) in [(-1, -1), (-1, 0), (-1, 1), (0, -1), (0, 1), (1, -1), (1, 0), (1, 1)]:
                currX = x + a
                currY = y + b
                if currX < 0 or currY < 0 or currX >= len(l) or currY >= len(l[x]):
                    pass
                elif l[currX][currY] == "#":
                    occupied += 1
            if l[x][y] == "L" and occupied == 0:
                output[x][y] = "#"
            elif l[x][y] == "#" and occupied >= 4:
                output[x][y] = "L"
    return output

data = []

for line in open("input.txt", "r"):
    data.append([x for x in line[:-1]])

while True:
    newData = nextRound(data)
    if myCheck(data, newData):
        data = copy.deepcopy(newData)
        break
    data = copy.deepcopy(newData)

print(count(data))