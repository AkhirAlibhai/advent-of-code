def canSum(x, l):
    for i in range(0, len(l)):
        for j in range(i + 1, len(l)):
            if l[i] + l[j] == x:
                return True
    return False

data = []

for line in open("input.txt", "r"):
    data.append(int(line[:-1]))

for x in range(25, len(data)):
    if not canSum(data[x], data[x - 25: x]):
        print(data[x])
        break