def canSum(x, l):
    for i in range(0, len(l)):
        for j in range(i + 1, len(l)):
            if l[i] + l[j] == x:
                return True
    return False

data = []

for line in open("input.txt", "r"):
    data.append(int(line[:-1]))

invalidNum = 0

for x in range(25, len(data)):
    if not canSum(data[x], data[x - 25: x]):
        invalidNum = data[x]
        break


for x in range(0, len(data) + 1):
    for y in range(0, len(data) + 1):
        if sum(data[x:y]) == invalidNum:
            print(min(data[x:y]) + max(data[x:y]))