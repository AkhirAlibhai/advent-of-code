import queue

data = []

for line in open("input.txt", "r"):
    data = [int(x) for x in line.split(",")]

spoken = {}
firstSpoken = set()

for x in range(0, len(data)):
    spoken[data[x]] = queue.Queue(2)
    spoken[data[x]].put(x)
    if data[x] not in firstSpoken:
        firstSpoken.add(data[x])
    else:
        firstSpoken.remove(data[x])

lastSpoken = data[x]

for x in range(len(data), 30000000):
    if lastSpoken in firstSpoken:
        if spoken[0].full():
            spoken[0].get()
        spoken[0].put(x)
        if 0 in firstSpoken:
            firstSpoken.remove(0)
        lastSpoken = 0
    else:
        newSpeak = abs(spoken[lastSpoken].queue[0] - spoken[lastSpoken].queue[1])

        if newSpeak in firstSpoken:
            firstSpoken.remove(newSpeak)

        if not newSpeak in spoken:
            spoken[newSpeak] = queue.Queue(2)
            firstSpoken.add(newSpeak)

        if spoken[newSpeak].full():
            spoken[newSpeak].get()

        spoken[newSpeak].put(x)
        lastSpoken = newSpeak

print(lastSpoken)