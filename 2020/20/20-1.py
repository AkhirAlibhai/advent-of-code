class Tile:
    def __init__(self, id, data):
        self.id = id
        self.data = data

        self.sides = {}
        self.sides["top"] = data[0]
        self.sides["bottom"] = data[len(data) - 1]

        self.sides["left"] = "".join([x[0] for x in data])
        self.sides["right"] = "".join([x[len(x) - 1] for x in data])

        self.possibles = []

    def isMatch(self, checkTile):
        for side in self.sides:
            for currSide in checkTile.sides:
                if self.sides[side] == checkTile.sides[currSide] or self.sides[side][::-1] == checkTile.sides[currSide]:
                    self.possibles.append(checkTile.id)

    def getSize(self):
        return len(self.possibles)

data = []

for line in open("input.txt", "r"):
    data.append(line.strip("\n"))

tiles = {}

newBlock = True
currId = ""
currData = []
for line in data:
    if newBlock:
        currId = int(line.split(" ")[1].strip(":"))
        newBlock = False
    elif line == "":
        tiles[currId] = Tile(currId, currData)
        newBlock = True
        currData = []
    else:
        currData.append(line)
tiles[currId] = Tile(currId, currData)

[tiles[x].isMatch(tiles[y]) for x in tiles for y in tiles if x != y]

output = 1
for x in [x for x in tiles if tiles[x].getSize() == 2]:
    output *= x

print(output)
