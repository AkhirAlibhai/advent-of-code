import math
import numpy as np

class Tile:
    def __init__(self, id, data):
        self.id = id
        self.data = np.array([[x == '#' for x in y] for y in data], dtype=np.uint8)

        self.sides = {}
        self.calculateEdges()
        self.possibles = []

    def calculateEdges(self):
        self.sides["top"] = self.data[0]
        self.sides["bottom"] = self.data[-1]

        self.sides["left"] = self.data.T[0]
        self.sides["right"] = self.data.T[-1]

    def isMatch(self, checkTile):
        for side in self.sides:
            for currSide in checkTile.sides:
                if np.array_equal(self.sides[side], checkTile.sides[currSide]) or np.array_equal(np.flip(self.sides[side]), checkTile.sides[currSide]):
                    self.possibles.append(checkTile.id)

    def findMatch(self, line):
        for side in self.sides:
            if np.array_equal(self.sides[side], line):
                return (side, "")
            elif np.array_equal(np.flip(self.sides[side]), line):
                return (side, "flip")
        return (None, None)

    def getSize(self):
        return len(self.possibles)

    def rotateClockwise(self):
        self.data = np.rot90(self.data, 3)
        self.calculateEdges()

    def rotateCounterClockwise(self):
        self.rotateClockwise()
        self.rotateClockwise()
        self.rotateClockwise()

    def flipVertically(self):
        self.data = np.fliplr(self.data)
        self.calculateEdges()

    def flipHorizontally(self):
        self.data = np.flipud(self.data)
        self.calculateEdges()

def findContenders(currId):
    foundMatches = {}
    for contender in tiles[currId].possibles:
        for side in tiles[currId].sides:
            possibleMatch = tiles[contender].findMatch(tiles[currId].sides[side])
            if possibleMatch != (None, None):
                foundMatches[side] = (contender, possibleMatch)

    return foundMatches

data = []

for line in open("input.txt", "r"):
    data.append(line.strip("\n"))

tiles = {}

newBlock = True
currId = ""
currData = []
for line in data:
    if newBlock:
        currId = int(line.split(" ")[1].strip(":"))
        newBlock = False
    elif line == "":
        tiles[currId] = Tile(currId, currData)
        newBlock = True
        currData = []
    else:
        currData.append(line)
tiles[currId] = Tile(currId, currData)

[tiles[x].isMatch(tiles[y]) for x in tiles for y in tiles if x != y]
corners = [x for x in tiles if tiles[x].getSize() == 2]

currId = corners[0]
prevId = -1

dimensions = math.sqrt(len(tiles))

image = {}

firstTime = True
newRow = False
count = 0
while True:
    if len(image) == dimensions**2:
        break

    if newRow:
        newRow = False
        currDirection = "top"
    else:
        currDirection = "left"

    if firstTime:
        firstTime = False
        for x in range(0, 8):
            foundMatches = findContenders(currId)
            if "right" in foundMatches.keys() and "bottom" in foundMatches.keys():
                image[count] = tiles[currId]
                prevId = currId
                currId = [foundMatches[x][0] for x in foundMatches if x == "right"][0]
                count += 1
                break
            else:
                tiles[currId].rotateClockwise()
                if x == 3:
                    tiles[currId].flipVertically()
    else:
        while True:
            foundMatches = findContenders(currId)
            if currDirection in foundMatches.keys() and foundMatches[currDirection][0] == prevId:
                if foundMatches[currDirection][1][1] == "flip":
                    if currDirection == "left":
                        tiles[currId].flipHorizontally()
                    else:
                        tiles[currId].flipVertically()
                else:
                    image[count] = tiles[currId]
                    count += 1
                    prevId = currId
                    try:
                        currId = [foundMatches[x][0] for x in foundMatches if x == "right"][0]
                    except:
                        foundMatches = findContenders(image[count - dimensions].id)
                        try:
                            prevId = image[count - dimensions].id
                            currId = [foundMatches[x][0] for x in foundMatches if x == "bottom"][0]
                            newRow = True
                        except:
                            pass
                    break
            else:
                tiles[currId].rotateClockwise()

pictureDim = len(image[0].data) - 2
picture = np.zeros((pictureDim * int(dimensions), pictureDim * int(dimensions)))

for x in range(0, int(dimensions)):
    for y in range(0, int(dimensions)):
        for lazy in range(0, pictureDim):
            for lazier in range(0, pictureDim):
                picture[x*pictureDim + lazy, y*pictureDim + lazier] = image[(x*int(dimensions)) + y].data[lazy + 1, lazier + 1]

pictureAsString = []
for x in picture:
    currLine = ""
    for y in x:
        currLine += "#" if y else "."
    pictureAsString.append(currLine)

finalImage = Tile(0, pictureAsString)
currMax = -1

monster = ["                  # ", "#    ##    ##    ###", " #  #  #  #  #  #   "]
monster = np.array([[x == '#' for x in y] for y in monster], dtype=np.uint8)
poundCount = list(finalImage.data.flatten()).count(True)

for z in range(0, 8):
    currCount = 0
    for x in range(0, pictureDim * int(dimensions) - len(monster)):
        for y in range(0, pictureDim * int(dimensions) - len(monster[0])):
            currWindow = finalImage.data[x:x+len(monster), y:y+len(monster[0])]

            if (currWindow & monster == monster).all():
                currCount += 1

    if currCount > currMax:
        currMax = currCount

    finalImage.rotateClockwise()
    if z == 3:
        finalImage.flipVertically()

print(poundCount - currMax*15)