data = {}
data[0] = {}

y = 0
for line in open("input.txt", "r"):
    splitLine = line.strip("\n")

    if y not in data[0]:
        data[0][y] = {}

    for x in range(0, len(splitLine)):
        data[0][y][x] = splitLine[x]
    y += 1

possibleDims = [-1, 0, 1]
dims = set([(z, y, x) for z in possibleDims for y in possibleDims for x in possibleDims])

dims.remove((0, 0, 0))

for currRound in range(0, 6):
    newData = {}

    currMins = (min(data.keys()) - 1, min(data[0].keys()) - 1, min(data[0][0].keys()) - 1)
    currMaxs = (max(data.keys()) + 1, max(data[0].keys()) + 1, max(data[0][0].keys()) + 1)

    for z in range(currMins[0], currMaxs[0] + 1):
        if z not in newData:
            newData[z] = {}

        for y in range(currMins[1], currMaxs[1] + 1):
            if y not in newData[z]:
                newData[z][y] = {}
            for x in range(currMins[2], currMaxs[2] + 1):
                active = 0
                for dim in dims:
                    try:
                        if data[z + dim[0]][y + dim[1]][x + dim[2]] == "#":
                            active += 1
                    except:
                        pass
                try:
                    if (data[z][y][x] == "#" and (active == 2 or active == 3)) or (data[z][y][x] == "." and active == 3):
                        newData[z][y][x] = "#"
                    else:
                        newData[z][y][x] = "."
                except:
                    if active == 3:
                        newData[z][y][x] = "#"
                    else:
                        newData[z][y][x] = "."

    data = newData

print(sum([1 for z in data for y in data[z] for x in data[z][y] if data[z][y][x] == "#"]))
