validPassports = 0

foundFields = set()

tags = {"byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid", "cid"}

for line in open("input.txt", "r"):
    if line == '\n':
        # We have a new passport

        # If the old one was valid, at it to the tally
        differentTags = tags.difference(foundFields)
        if len(differentTags) == 0 or differentTags == {"cid"}:
            validPassports += 1
        foundFields = set()
    else:
        splitLine = line[:-1].split(" ")
        for split in splitLine:
            foundFields.add(split.split(":")[0])

# Catching the last one
differentTags = tags.difference(foundFields)
if len(differentTags) == 0 or ("cid" in differentTags and len(differentTags) == 1):
    validPassports += 1

print(validPassports)