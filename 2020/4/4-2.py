import re

def byr(year):
    if len(year) != 4:
        return False
    return 1920 <= int(year) and int(year) <= 2002

def iyr(year):
    if len(year) != 4:
        return False
    return 2010 <= int(year) and int(year) <= 2020

def eyr(year):
    if len(year) != 4:
        return False
    return 2020 <= int(year) and int(year) <= 2030

def hgt(height):
    if (height[-2:] == "cm"):
        heightVal = int(height[:-2])
        return 150 <= heightVal and heightVal <= 193
    elif (height[-2:] == "in"):
        heightVal = int(height[:-2])
        return 59 <= heightVal and heightVal <= 76
    return False

def hcl(hairColour):
    return re.search(r'^#(?:[0-9a-f]{3}){1,2}$', hairColour)

def ecl(eyeColour):
    return eyeColour in {"amb", "blu", "brn", "gry", "grn", "hzl", "oth"}

def pid(id):
    if len(id) != 9:
        return False
    try:
        int(id)
        return True
    except:
        return False

def cid(cid):
    True

validPassports = 0

foundFields = set()

tags = {"byr": byr, "iyr": iyr, "eyr": eyr, "hgt": hgt, "hcl": hcl, "ecl": ecl, "pid": pid, "cid": cid}
tagsNoFuncs = set(tags.keys())

for line in open("input.txt", "r"):
    if line == '\n':
        # We have a new passport

        # If the old one was valid, at it to the tally
        differentTags = tagsNoFuncs.difference(foundFields)
        if len(differentTags) == 0 or differentTags == {"cid"}:
            validPassports += 1
        foundFields = set()
    else:
        splitLine = line[:-1].split(" ")
        for split in splitLine:
            splitVals = split.split(":")
            if tags[splitVals[0]](splitVals[1]):
                foundFields.add(splitVals[0])

# Catching the last one
differentTags = tagsNoFuncs.difference(foundFields)
if len(differentTags) == 0 or ("cid" in differentTags and len(differentTags) == 1):
    validPassports += 1

print(validPassports)