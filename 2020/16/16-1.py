class myRange:
    def __init__(self, minVal1, maxVal1, minVal2, maxVal2):
        self.minVal1 = minVal1
        self.maxVal1 = maxVal1

        self.minVal2 = minVal2
        self.maxVal2 = maxVal2
    
    def __contains__(self, val):
        return (self.minVal1 <= val and val <= self.maxVal1) or (self.minVal2 <= val and val <= self.maxVal2)

data = []

for line in open("input.txt", "r"):
    data.append(line.strip("\n"))

invalidTickets = []
x = 0

ranges = []
while data[x] != '':
    currLine = data[x].split(": ")
    currRanges = currLine[1].split(" or ")

    range1 = currRanges[0].split("-")
    min1 = int(range1[0])
    max1 = int(range1[1])

    range2 = currRanges[1].split("-")
    min2 = int(range2[0])
    max2 = int(range2[1])

    ranges.append(myRange(min1, max1, min2, max2))
    x += 1

x += 1

# Skipping my ticket
while data[x] != '':
    x += 1

for ticket in data[x + 2:]:
    for currSplitTicket in ticket.split(","):
        splitTicket = int(currSplitTicket)
        invalidTicket = [1 for x in ranges if splitTicket in x]
        if len(invalidTicket) == 0:
            invalidTickets.append(splitTicket)

print(sum(invalidTickets))