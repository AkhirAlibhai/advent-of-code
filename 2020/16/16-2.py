class myRange:
    def __init__(self, minVal1, maxVal1, minVal2, maxVal2, tag):
        self.minVal1 = minVal1
        self.maxVal1 = maxVal1

        self.minVal2 = minVal2
        self.maxVal2 = maxVal2

        self.tag = tag

    def __contains__(self, val):
        return (self.minVal1 <= val and val <= self.maxVal1) or (self.minVal2 <= val and val <= self.maxVal2)

data = []

for line in open("input.txt", "r"):
    data.append(line.strip("\n"))

invalidTickets = []
x = 0

ranges = []
while data[x] != '':
    currLine = data[x].split(": ")
    currRanges = currLine[1].split(" or ")

    range1 = currRanges[0].split("-")
    min1 = int(range1[0])
    max1 = int(range1[1])

    range2 = currRanges[1].split("-")
    min2 = int(range2[0])
    max2 = int(range2[1])

    ranges.append(myRange(min1, max1, min2, max2, currLine[0]))
    x += 1
x += 2

myTicket = [int(y) for y in data[x].split(",")]
validTickets = []

for ticket in data[x + 3:]:
    isInvalid = False
    for currSplitTicket in ticket.split(","):
        splitTicket = int(currSplitTicket)
        invalidTicket = [1 for x in ranges if splitTicket in x]
        if len(invalidTicket) == 0:
            isInvalid = True
            break
    if not isInvalid:
        validTickets.append([int(x) for x in ticket.split(",")])

tagCount = {}
for ticket in validTickets:
    for x in range(0, len(ticket)):
        for currRange in ranges:
            if ticket[x] in currRange:
                if x not in tagCount:
                    tagCount[x] = {}
                if currRange.tag not in tagCount[x]:
                    tagCount[x][currRange.tag] = 0
                tagCount[x][currRange.tag] += 1

tagOrder = {}
remainingTags = set([x.tag for x in ranges])
while len(remainingTags) > 0:
    for x in tagCount:
        currTags = {}

        for tag in tagCount[x]:
            if tag in remainingTags:
                if tagCount[x][tag] not in currTags:
                    currTags[tagCount[x][tag]] = []
                currTags[tagCount[x][tag]].append(tag)

        if len(currTags.keys()) != 0:
            currMax = max(currTags.keys())
            if len(currTags[currMax]) == 1 and x not in tagOrder:
                tagOrder[x] = currTags[currMax][0]
                remainingTags.remove(currTags[currMax][0])

answer = 1
for x in tagOrder:
    if tagOrder[x].startswith("departure"):
        print(x)
        answer *= myTicket[x]

print(answer)
