validPasswords = 0

for line in open("input.txt", "r"):
  lineChunk = line.split(" ")
  lineChunk[0] = lineChunk[0].split("-")

  firstIndex = int(lineChunk[0][0]) - 1
  secondIndex = int(lineChunk[0][1]) - 1

  checkedLetter = lineChunk[1][0]

  if (lineChunk[2][firstIndex] == checkedLetter or lineChunk[2][secondIndex] == checkedLetter) and (lineChunk[2][firstIndex] != lineChunk[2][secondIndex]):
        validPasswords = validPasswords + 1

print(validPasswords)