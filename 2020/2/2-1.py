validPasswords = 0

for line in open("input.txt", "r"):
  lineChunk = line.split(" ")
  lineChunk[0] = lineChunk[0].split("-")

  lineMin = int(lineChunk[0][0])
  lineMax = int(lineChunk[0][1])

  checkedLetter = lineChunk[1][0]

  count = 0
  for letter in lineChunk[2]:
    if checkedLetter == letter:
        count = count + 1

  if lineMin <= count and count <= lineMax:
      validPasswords = validPasswords + 1

print(validPasswords)