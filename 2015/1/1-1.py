output = 0

for line in open("input.txt", "r"):
    output += sum([1 for x in line if x == "("])
    output -= sum([1 for x in line if x == ")"])

print(output)