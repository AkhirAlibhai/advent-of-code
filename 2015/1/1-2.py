data = ''
for line in open("input.txt", "r"):
    data = line.strip('\n')

currFloor = 0
for x in range(0, len(data)):
    if data[x]  == "(":
        currFloor += 1
    elif data[x] == ")":
        currFloor -= 1

    if currFloor == -1:
        print(x + 1)
        break
