calorie_list = []

curr_elf = 0
for line in open("input.txt", "r"):
    if line == "\n":
        calorie_list.append(curr_elf)
        curr_elf = 0
    else:
        curr_elf += int(line.strip())


output = 0
for x in range(0, 3):
    curr_max = max(calorie_list)
    output += max(calorie_list)
    calorie_list.remove(curr_max)

print(output)
