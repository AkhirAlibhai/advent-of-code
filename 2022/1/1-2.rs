use std::fs::File;
use std::io::{self, prelude::*, BufReader};

fn main() -> io::Result<()> {
    let file = File::open("input.txt")?;
    let input = BufReader::new(file);

    let mut calorie_list = Vec::new();
    let mut curr_elf = 0;

    for line in input.lines() {
        let value = line?;
        if value == "" {
            calorie_list.push(curr_elf);
            curr_elf = 0;
        }
        else {
            curr_elf += value.parse::<i32>().unwrap();
        }
    }

    let mut output = 0;
    calorie_list.sort();

    output += calorie_list.pop().unwrap();
    output += calorie_list.pop().unwrap();
    output += calorie_list.pop().unwrap();

    println!("{}", output);

    Ok(())
}