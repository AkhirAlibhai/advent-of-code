calorie_list = []

curr_elf = 0
for line in open("input.txt", "r"):
    if line == "\n":
        calorie_list.append(curr_elf)
        curr_elf = 0
    else:
        curr_elf += int(line.strip())

print(max(calorie_list))
