score_mapping = {0: 1, 1: 2, 2: 3}

LOST_GAME = 0
DRAW_GAME = 3
WIN_GAME = 6


def match_score(opponent: str, player: str) -> int:
    opponent = ord(opponent) % ord("A")
    player = ord(player) % ord("X")
    score = score_mapping[player]

    if player == 0:
        # Need to lose
        return LOST_GAME + score_mapping[(opponent - 1) % 3]

    if player == 1:
        # Need to draw
        return DRAW_GAME + score_mapping[opponent]

    # Need to win
    return WIN_GAME + score_mapping[((opponent + 1) % 3)]


output = 0
for line in open("input.txt", "r"):
    output += match_score(*line.strip().split(" "))

print(output)
