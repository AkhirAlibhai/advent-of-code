score_mapping = {0: 1, 1: 2, 2: 3}

LOST_GAME = 0
DRAW_GAME = 3
WIN_GAME = 6


def match_score(opponent: str, player: str) -> int:
    opponent = ord(opponent) % ord("A")
    player = ord(player) % ord("X")
    score = score_mapping[player]

    if opponent == player:
        return DRAW_GAME + score

    if (opponent + 1) % 3 == player:
        return WIN_GAME + score

    return LOST_GAME + score


output = 0
for line in open("input.txt", "r"):
    output += match_score(*line.strip().split(" "))

print(output)
