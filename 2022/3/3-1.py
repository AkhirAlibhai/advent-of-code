def compartment_priority(rucksack: str) -> int:
    compartment_length = len(rucksack)//2
    second_compartment = rucksack[compartment_length:]

    # There should only be 1, can just grab the first index
    priority = ord(list(filter(lambda character: character in second_compartment, rucksack[:compartment_length]))[0])

    if priority >= 97:
        return priority - ord('a') + 1

    return priority - ord('A') + 27

output = 0
for line in open("input.txt", "r"):
    output += compartment_priority(line.strip("\n"))

print(output)
