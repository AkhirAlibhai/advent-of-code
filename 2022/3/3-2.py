def badge_priority(
    first_rucksack: str, second_rucksack: str, third_rucksack: str
) -> int:
    # There should only be 1, can just grab the first index
    priority = ord(
        list(
            filter(
                lambda character: character in second_rucksack
                and character in third_rucksack,
                first_rucksack,
            )
        )[0]
    )

    if priority >= 97:
        return priority - ord("a") + 1

    return priority - ord("A") + 27


output = 0
curr_lines = []
counter = 0
for line in open("input.txt", "r"):
    curr_lines.append(line.strip("\n"))
    counter += 1

    if counter == 3:
        output += badge_priority(*curr_lines)
        curr_lines = []
        counter = 0

print(output)
