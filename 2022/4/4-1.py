class Section:
    def __init__(self, input: str) -> None:
        split_input = input.split("-")
        self.start = int(split_input[0])
        self.end = int(split_input[1])

    def is_one_in_another(self, second) -> bool:
        print(self.start, self.end, second.start, second.end)
        if self.start <= second.start and second.end <= self.end:
            return True

        if second.start <= self.start and self.end <= second.end:
            return True

        return False


output = 0
for line in open("input.txt", "r"):
    first, second = line.strip().split(",")
    output += int(Section(first).is_one_in_another(Section(second)))

print(output)
