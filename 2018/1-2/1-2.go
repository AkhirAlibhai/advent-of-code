package main

import (
  "fmt"
  "io/ioutil"
  "strings"
  "strconv"
)

func main() {
  isDone := false

  visitedFrequencies := map[int]bool{
    0 : true,
  }

  fileData, err := ioutil.ReadFile("data.txt")
  if err != nil {
    fmt.Print(err)
  }

  frequency := 0

  data := string(fileData)
  splitData := strings.Split(data, "\n")

  for {
    for i := 0; i < len(splitData); i++ {
      value, err := strconv.Atoi(splitData[i])

      if err != nil {
        fmt.Print(err)
      }

      frequency += value

      if visitedFrequencies[frequency] {
        fmt.Print(frequency)

        isDone = true

        break
      } else {
        visitedFrequencies[frequency] = true
      }
    }

    if isDone {
      break
    }
  }
}

