package main

import (
  "fmt"
  "io/ioutil"
  "strings"
  "strconv"
)

func main() {
  fileData, err := ioutil.ReadFile("data.txt")
  if err != nil {
    fmt.Print(err)
  }

  fabricMap := make(map[[2]int]string)
  idMap := make(map[string]bool)

  data := string(fileData)
  splitData := strings.Split(data, "\n")

  for i := 0; i < len(splitData); i++ {
    currLine := strings.Split(splitData[i], "@")
    currLineSplit := strings.Split(currLine[1], ":")

    currID := strings.TrimSpace(currLine[0])
    idMap[currID] = true

    //I did not want to deal with error handling on this

    currPoint := strings.Split(currLineSplit[0], ",")
    currPointX, _ := strconv.Atoi(strings.TrimSpace(currPoint[0]))
    currPointY, _ := strconv.Atoi(strings.TrimSpace(currPoint[1]))

    currArea := strings.Split(currLineSplit[1], "x")
    currAreaX, _ := strconv.Atoi(strings.TrimSpace(currArea[0]))
    currAreaY, _ := strconv.Atoi(strings.TrimSpace(currArea[1]))

    for x := currPointX; x < currPointX + currAreaX; x++ {
      for y := currPointY; y < currPointY + currAreaY; y++ {
        if fabricMap[[2]int{x,y}] == "" {
          fabricMap[[2]int{x,y}] = currID
        } else {
          idMap[fabricMap[[2]int{x,y}]] = false
          idMap[currID] = false
        }
      }
    }
  }

  for key, value := range idMap {
    if value {
      fmt.Print(key)
    }
  }
}
