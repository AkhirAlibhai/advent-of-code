package main

import(
  "fmt"
  "io/ioutil"
  "strings"
)

func reactPolymer(polymer []byte) []string {
  result := make([]string, 0, len(polymer))

  wentBack := false
  prevUnit := polymer[0]

  for i := 1; i < len(polymer); i++ {
    currUnit := polymer[i]

    if currUnit + 32 == prevUnit || currUnit == prevUnit + 32 {
      if wentBack {
        if len(result) != 0 {
          result = result[:len(result) - 1]
        }
      }
      if len(result) != 0 {
        prevUnit = []byte(result[len(result) - 1])[0]
        wentBack = true
      } else {
        i++
        prevUnit = polymer[i]
        wentBack = false
      }
    } else {
      if !wentBack {
        result = append(result, string(prevUnit))
      }
      prevUnit = currUnit
      wentBack = false

      if i == len(polymer) - 1 {
        result = append(result, string(currUnit))
      }
    }
  }
  return result
}

func main() {
  fileData, err := ioutil.ReadFile("data.txt")
  if err != nil {
    fmt.Print(err)
  }

  data := string(fileData)

  alphaLower := "abcdefghijklmnopqrstuvwxyz"
  alphaUpper := "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

  bestLen := len(data) + 1

  for i := 0; i < len(alphaLower); i++ {
    currData := strings.Replace(data, string(alphaLower[i]), "", -1)
    currData = strings.Replace(currData, string(alphaUpper[i]), "", -1)

    result := reactPolymer([]byte(currData))

    if len(result) < bestLen {
      bestLen = len(result)
    }
  }

  fmt.Print(bestLen)
}
