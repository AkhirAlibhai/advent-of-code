package main

import (
  "fmt"
  "io/ioutil"
  "strings"
  "time"
  "strconv"
)

func merge(leftArray []time.Time, rightArray []time.Time) []time.Time {
  sortedArray := make([]time.Time, len(leftArray) + len(rightArray))

  leftCounter := 0
  rightCounter := 0

  for currIndex := 0; currIndex < len(leftArray) + len(rightArray); currIndex++ {
    if leftCounter == len(leftArray) {
      sortedArray[currIndex] = rightArray[rightCounter]
      rightCounter++
    } else if rightCounter == len(rightArray) {
      sortedArray[currIndex] = leftArray[leftCounter]
      leftCounter++
    } else {
       if leftArray[leftCounter].Before(rightArray[rightCounter]) {
        sortedArray[currIndex] = leftArray[leftCounter]
        leftCounter++
      } else {
        sortedArray[currIndex] = rightArray[rightCounter]
        rightCounter++
      }
    }
  }

  return sortedArray
}

func mergeSort(timeArray []time.Time) []time.Time {
  if len(timeArray) <= 1 {
    return timeArray
  }

  leftArray := mergeSort(timeArray[:len(timeArray)/2])
  rightArray := mergeSort(timeArray[len(timeArray)/2:])

  return merge(leftArray, rightArray)
}

func main() {
  fileData, err := ioutil.ReadFile("data.txt")
  if err != nil {
    fmt.Print(err)
  }

  timeMap := make(map[time.Time]string)

  data := string(fileData)
  splitData := strings.Split(data, "\n")

  layout := "2006-01-02 15:04"
  
  for i := 0; i < len(splitData); i++ {
    currLine := strings.Split(splitData[i], "]")
    
    parseTime, _ := time.Parse(layout, currLine[0][1:])
    
    timeMap[parseTime] = strings.TrimSpace(currLine[1])
  }
  
  sortedTimes := make([]time.Time, len(timeMap))

  counter := 0
  for key := range timeMap {
    sortedTimes[counter] = key
    counter++
  }

  //Sorting the map
  sortedTimes = mergeSort(sortedTimes)

  //Buliding a map for each guard's schedule
  guardMap := make(map[int][60]int)

  currGuard := 0
  currStart := 0
  currEnd := 0

  for i := 0; i < len(sortedTimes); i++ {
    currLine := timeMap[sortedTimes[i]]
    if currLine[0] == 'G' {
      currGuard, _ = strconv.Atoi(strings.Split(currLine, " ")[1][1:])
    } else if currLine[0] == 'f' {
      currStart, _ = strconv.Atoi(strings.Split(sortedTimes[i].Format(layout), " ")[1][3:])
    } else {
      currEnd, _ = strconv.Atoi(strings.Split(sortedTimes[i].Format(layout), " ")[1][3:])
      
      for minute := currStart; minute < currEnd; minute++ {
        currMap := guardMap[currGuard]
        currMap[minute]++
        
        guardMap[currGuard] = currMap
      }
    }
  }

  currHighestGuard := 0
  currHighestMinutes := 0
  currOftenMinute := 0

  for guard := range guardMap {
    currMinutes := 0
    oftenMinute := 0
  
    for minute := 0; minute < 60; minute++ {
      currMinutes+= guardMap[guard][minute]
      
      if guardMap[guard][minute] > guardMap[guard][oftenMinute] {
        oftenMinute = minute
      }
    }
    
    if currMinutes > currHighestMinutes {
      currHighestMinutes = currMinutes
      currHighestGuard = guard
      currOftenMinute = oftenMinute
    }
  }

  fmt.Print(currHighestGuard * currOftenMinute)
}
