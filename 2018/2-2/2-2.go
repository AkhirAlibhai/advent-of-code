package main

import (
  "fmt"
  "io/ioutil"
  "strings"
)

func main() {
  fileData, err := ioutil.ReadFile("data.txt")
  if err != nil {
    fmt.Print(err)
  }

  data := string(fileData)
  splitData := strings.Split(data, "\n")

  for i := 0; i < len(splitData); i++ {
    for j := i + 1; j < len(splitData); j++ {
      oneDifference := false

      for k := 0; k < len(splitData[i]); k++ {
        if splitData[i][k] != splitData[j][k] {
          if oneDifference {
            oneDifference = false
            break
          }
          oneDifference = true
        }
      }

      if oneDifference {
        fmt.Printf("%s - %s", splitData[i], splitData[j])
        break
      }
    }
  }
}