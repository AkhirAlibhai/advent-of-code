package main

import (
  "fmt"
  "io/ioutil"
  "strings"
  "strconv"
)

func main() {
  fileData, err := ioutil.ReadFile("data.txt")
  if err != nil {
    fmt.Print(err)
  }

  frequency := 0

  data := string(fileData)
  splitData := strings.Split(data, "\n")

  for i := 0; i < len(splitData); i++ {
    value, err := strconv.Atoi(splitData[i])

    if err != nil {
      fmt.Print(err)
    }

    frequency += value
  }
  fmt.Print(frequency)
}
