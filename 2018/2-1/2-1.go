package main

import (
  "fmt"
  "io/ioutil"
  "strings"
)

func main() {
  fileData, err := ioutil.ReadFile("data.txt")
  if err != nil {
    fmt.Print(err)
  }

  twoLetters := 0
  threeLetters := 0

  data := string(fileData)
  splitData := strings.Split(data, "\n")

  for i := 0; i < len(splitData); i++ {
    letterMap := make(map[byte]int)
    twoLocal := 0
    threeLocal := 0

    for j := 0; j < len(splitData[i]); j++ {
      if letterValue, exists := letterMap[splitData[i][j]]; exists {
        if letterValue == 1 {
          twoLocal++
        } else if letterValue == 2 {
          twoLocal--
          threeLocal++
        } else if letterValue == 3 {
          threeLocal--
        }
        letterMap[splitData[i][j]]++
      } else {
        letterMap[splitData[i][j]] = 1
      }
    }

    if twoLocal > 0 {
      twoLetters++
    }
    if threeLocal > 0 {
      threeLetters++
    }
  }
  fmt.Printf("Checksum: %d\n", twoLetters*threeLetters)
}