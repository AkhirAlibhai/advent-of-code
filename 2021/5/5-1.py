from collections import defaultdict

grid = defaultdict()

for line in open("input.txt", "r"):
  splitLine = line.strip("\n").split(" -> ")
  startX, startY = [int(x) for x  in splitLine[0].split(",")]
  endX, endY = [int(x) for x in splitLine[1].split(",")]

  if startX == endX or startY == endY:
    if startY > endY:
      tmp = startY
      startY = endY
      endY = tmp
    if startX > endX:
      tmp = startX
      startX = endX
      endX = tmp

    for x in range(startX, endX + 1):
      if x not in grid:
        grid[x] = defaultdict(int)
      for y in range(startY, endY + 1):
        grid[x][y] += 1


print(sum([1 for x in grid for y in grid[x] if grid[x][y] >= 2]))
