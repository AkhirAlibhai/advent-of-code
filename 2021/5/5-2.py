from collections import defaultdict

grid = defaultdict()

for line in open("input.txt", "r"):
  splitLine = line.strip("\n").split(" -> ")
  startX, startY = [int(x) for x  in splitLine[0].split(",")]
  endX, endY = [int(x) for x in splitLine[1].split(",")]

  if startY > endY:
    yRange = range(startY, endY - 1, -1)
  else:
    yRange = range(startY, endY + 1)
  if startX > endX:
    xRange = range(startX, endX - 1, -1)
  else:
    xRange = range(startX, endX + 1)

  if startX == endX or startY == endY:
    for x in xRange:
      if x not in grid:
        grid[x] = defaultdict(int)
      for y in yRange:
        grid[x][y] += 1
  else:
    for x, y in zip(xRange, yRange):
      if x not in grid:
        grid[x] = defaultdict(int)
      grid[x][y] += 1

print(sum([1 for x in grid for y in grid[x] if grid[x][y] >= 2]))