result = 0

curr = None

for line in open("input.txt", "r"):
  if not curr:
      curr = int(line)
  else:
    prev = curr
    curr = int(line)
    

    if curr > prev:
      result += 1


print(result)