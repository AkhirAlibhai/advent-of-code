data = []

for line in open("input.txt", "r"):
  data.append(int(line))

windowSize = 3

sums = []
result = 0
currWindow = sum(data[0:3])

for x in range(1, len(data) - windowSize + 1):
  prevWindow = currWindow
  currWindow = currWindow - data[x-1] + data[x+windowSize-1]
  if currWindow > prevWindow:
    result += 1

print(result)