from collections import defaultdict

input = []

for line in open("input.txt", "r"):
  input.append(line.strip("\n"))

currOxygen = 0
currScrubber = 0

oxygen = list(input)
scrubber = list(input)

while (len(oxygen) != 1 or len(scrubber) != 1):
  if len(oxygen) != 1:
    tally = 0
    for x in oxygen:
      if x[currOxygen] == "1":
        tally += 1
      else:
        tally -= 1

    oxygen = [oxygen[y] for y in range (0, len(oxygen)) if oxygen[y][currOxygen] == "1" and tally >= 0 or oxygen[y][currOxygen] == "0" and tally < 0]
    currOxygen += 1


  if len(scrubber) != 1:
    tally = 0
    for x in scrubber:
      if x[currScrubber] == "1":
        tally += 1
      else:
        tally -= 1

    scrubber = [scrubber[y] for y in range (0, len(scrubber)) if scrubber[y][currScrubber] == "0" and tally >= 0 or scrubber[y][currScrubber] == "1" and tally < 0]
    currScrubber += 1

print(int(oxygen[0], 2) * int(scrubber[0], 2))