from collections import defaultdict

tally = defaultdict(int)

def addTally(x, y):
  tally[x] += y

for line in open("input.txt", "r"):
  [addTally(x, 1 if line[x] == "1" else -1) for x in range(0, len(line) - 1)]

gamma = ""

for x in range(0, len(tally)):
  if tally[x] >= 0:
    gamma += "1"
  else:
    gamma += "0"

epsilon = "".join("1" if x == "0" else "0" for x in gamma)

print(int(gamma, 2) * int(epsilon, 2))
