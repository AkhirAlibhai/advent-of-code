currDepth = 0
currHorz = 0

for line in open("input.txt", "r"):
  x = line.split(" ")

  if x[0] == "forward":
    currHorz += int(x[1])
  elif x[0] == "down":
    currDepth += int(x[1])
  else:
    currDepth -= int(x[1])

result = currDepth * currHorz
print(result)