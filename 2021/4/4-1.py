class BingoBoard:
  def __init__(self):
      self.board = []

  def addRow(self, boardString):
      self.board.append(boardString.split())

  def checkBingo(self, currNum):
    for index, row in enumerate(self.board):
      if currNum in row:
        item = row.index(currNum)
        self.board[index][item] = None
        
        if self.board[index].count(None) == len(self.board[index]):
          return True
        
        for x in self.board:
          if x[item] != None:
            return False
        return True

    return False
  
  def sumUnmarked(self):
    return sum([int(item) for row in self.board for item in row if item != None])

  def __str__(self):
    output = ""
    for row in self.board:
      output += str(row) + "\n"
    return output

order = None

boards = set()
currBoard = None
for line in open("input.txt", "r"):
  if line.strip("\n") != "":
    if order == None:
      order = line.strip("\n").split(",")
    else:
      currBoard.addRow(line.strip("\n"))
  else:
    if currBoard != None:
      boards.add(currBoard)
    currBoard = BingoBoard()

boards.add(currBoard)

answerFound = False

for number in order:
  for board in boards:
    if board.checkBingo(number):
      answerFound = True
      print(int(number) * board.sumUnmarked())
      break
  if answerFound:
    break
